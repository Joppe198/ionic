import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp(functions.config().firebase)


export const updatelikesCount=functions.region('europe-west2').https.onRequest((request,response)=>{
console.log("request Body is:"+request.body);

const postId=JSON.parse(request.body).postId;
const userId=JSON.parse(request.body).userId;
const action=JSON.parse(request.body).action;

console.log('Post ID'+postId);
console.log('userId'+userId);
console.log('action'+action);

admin.firestore().collection("posts").doc(postId).get().then((data:any)=>{

let likesCount=data.data().likesCount || 0;




//let likes:any=data.data().likes||[];

// tslint:disable-next-line:prefer-const
let updateData:any={};

if(action==='like')
{
updateData["likesCount"]=++likesCount;
updateData[`likes.${userId}`]=true;
}
else{

    updateData["likesCount"]=--likesCount;
    updateData[`likes.${userId}`]=false;
}

admin.firestore().collection("posts").doc(postId).update(updateData).then(()=>{
response.status(200).send("done")
}).catch((err)=>{
response.status(err.code).send(err.message)
})




}).catch((err)=>{
console.log(err)
})


})

export const updateCommentsCount = functions.region('europe-west2').firestore.document('comments/{commentId}').onCreate(async (event) => {

  const data = event.data() || {};
  const postId = data.post;
  const doc = await admin.firestore().collection("posts").doc(postId).get();

  if(doc.exists){
      const docdata = doc.data() || {};
      let commentsCount = docdata.commentsCount || 0;
      commentsCount++;
      await admin.firestore().collection("posts").doc(postId).update({
          "commentsCount": commentsCount
      })
      return true;
  } else {
      return false;
  }

})
