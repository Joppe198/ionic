import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import firebase from 'firebase';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  name: string = "";
  email: string = "";
  password: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public alertCtrl: AlertController) {
  }

  signup(){
    firebase.auth().createUserWithEmailAndPassword(this.email, this.password)
    .then((data) => {
      console.log(data)
      let newUser: firebase.User = data.user;
      newUser.updateProfile({
        displayName: this.name,
        photoURL: ""
      }).then(() => {
        console.log("Profiili päivitetty")

        this.alertCtrl.create({
          title: "Tunnukset Luotu",
          message: "Tunnukset Luotu Onnistuneesti.",
          buttons: [
            {
              text: "OK",
              handler: () => {
                //Navigate to the feeds page
                this.navCtrl.setRoot(ChatPage)
              }
            }
          ]
        }).present();
      }).catch((err) => {
        console.log(err)
      })

    }).catch((err) => {
      console.log(err)
      this.toastCtrl.create({
        message: err.message,
        duration: 3000
      }).present();
    })
  }

  goBack(){
    this.navCtrl.pop();
  }
}
