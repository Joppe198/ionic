import { Component } from '@angular/core';
import { NavController, NavParams,ToastController } from 'ionic-angular';
import { ChatPage } from '../chat/chat';
import firebase from 'firebase';
import { SignupPage } from '../signup/signup';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string = "";
  password: string = "";

  constructor(public navCtrl: NavController, public toastCtrl: ToastController) {

  }

  login(){
    firebase.auth().signInWithEmailAndPassword(this.email, this.password)
    .then((user) => {
      console.log(user)
      this.toastCtrl.create({
        message: "Tervetuloa " + user.user.displayName,
        duration: 3000
      }).present();
      this.navCtrl.setRoot(ChatPage)
    }).catch((err) => {
      console.log(err)
      this.toastCtrl.create({
        message: err.message,
        duration: 3000
      }).present();
    })
  }

  gotoSignup(){
    this.navCtrl.push(SignupPage);
  }
}

