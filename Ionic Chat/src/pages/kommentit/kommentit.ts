import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import firebase from  'firebase';
import moment from 'moment';
import 'moment/locale/fi'


@Component({
  selector: 'page-kommentit',
  templateUrl: 'kommentit.html',
})
export class KommentitPage {

  post: any = {};
  comments : any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController) {

    this.post = this.navParams.get("post");
    console.log(this.post)
    firebase.firestore().collection("comments").where("post", "==", this.post.id).get()
    .then((data) => {
      this.comments = data.docs;
    }).catch((err) => {
      console.log(err)
    })
  }
  close(){
    this.viewCtrl.dismiss();
  }
  ago(time) {
    let difference = moment(time).diff(moment());
    return moment.duration(difference).humanize();
  }
}
